#include <EEPROM.h>

struct Producto {
  String nombre;
  int precio;
  int existencias;
};

Producto productos[10];

//----------------------------------------FUNCIONES NECESARIAS IMPRIMIR, GUARDAR Y LEER PRODUCTOS DESDE LA EEPROM--------------------------------------
void imprimirProducto(Producto prod) {
  Serial.println("------------------------------------------");
  Serial.print("Nombre: ");
  Serial.println(prod.nombre);

  Serial.print("Precio: Q");
  Serial.println(prod.precio);

  Serial.print("Existencias: ");
  Serial.println(prod.existencias);
  Serial.println("------------------------------------------");
}

void guardarProducto(Producto prod, int direccion) {
  // Guardar el nombre
  byte len = prod.nombre.length();
  EEPROM.write(direccion, len);
  for (int i = 0; i < len; i++) {
    EEPROM.write(direccion + 1 + i, prod.nombre.charAt(i));
  }

  // Guardar el precio
  EEPROM.put(direccion + 1 + prod.nombre.length(), prod.precio);

  // Guardar las existencias
  EEPROM.put(direccion + 1 + prod.nombre.length() + sizeof(prod.precio), prod.existencias);
}

Producto leerProducto(int direccion) {
  Producto prod;

  // Leer el nombre
  byte len = EEPROM.read(direccion);
  char nombre[len + 1];
  for (int i = 0; i < len; i++) {
    nombre[i] = EEPROM.read(direccion + 1 + i);
  }
  nombre[len] = '\0'; // Agregar el terminador nulo al final del string
  prod.nombre = String(nombre);

  // Leer el precio
  EEPROM.get(direccion + 1 + strlen(nombre), prod.precio);

  // Leer las existencias
  EEPROM.get(direccion + 1 + strlen(nombre) + sizeof(prod.precio), prod.existencias);

  return prod;
}
//---------------------------------------------------------------------------------------------------------------------------------------------------------

void setup() {
  Serial.begin(9600);
  //-----------------------PRODUCTO 1
  productos[0].nombre = "Jabon fabuloso";
  productos[0].precio = 10;
  productos[0].existencias = 0;

  //-----------------------PRODUCTO 2
  productos[1].nombre = "Sabritas";
  productos[1].precio = 8;
  productos[1].existencias = 20;

  //-----------------------PRODUCTO 3
  productos[2].nombre = "Chicles";
  productos[2].precio = 2;
  productos[2].existencias = 2;

  //-----------------------PRODUCTO 4
  productos[3].nombre = "Coca cola";
  productos[3].precio = 4;
  productos[3].existencias = 20;

  //-----------------------PRODUCTO 5
  productos[4].nombre = "Empanada";
  productos[4].precio = 12;
  productos[4].existencias = 10;

  //-----------------------PRODUCTO 6
  productos[5].nombre = "Jugo de limon";
  productos[5].precio = 3;
  productos[5].existencias = 25;

  //-----------------------PRODUCTO 7
  productos[6].nombre = "Sandwich";
  productos[6].precio = 10;
  productos[6].existencias = 12;

  //-----------------------PRODUCTO 8
  productos[7].nombre = "Cloro";
  productos[7].precio = 7;
  productos[7].existencias = 0;

  //-----------------------PRODUCTO 9
  productos[8].nombre = "Paquete de queso";
  productos[8].precio = 11;
  productos[8].existencias = 8;

  //-----------------------PRODUCTO 10
  productos[9].nombre = "Pan blanco";
  productos[9].precio = 9;
  productos[9].existencias = 4;

  for (int i = 0; i < 10; i++) {
    guardarProducto(productos[i], 400 + (i * 50));
    delay(100);
  }

  for (int i = 0; i < 10; i++) {
    Serial.println("Producto numero: " + String(i + 1));
    productos[i] = leerProducto(400 + (i * 50));
    imprimirProducto(productos[i]);
  }
}

void loop() {
  // put your main code here, to run repeatedly:

}