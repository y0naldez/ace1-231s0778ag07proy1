#include <EEPROM.h>
#include <LiquidCrystal.h>
#include <Keypad.h>
#include <LedControl.h>

#define btnAceptar 7
#define btnCancelar 6
#define btnDerecha 5
#define btnIzquierda 4

#define B1 43
#define B2 41
#define B3 39
#define B4 37

#define led 3

const byte ROWS = 4;
const byte COLS = 4;
char keys[ROWS][COLS] = {
  { '7', '8', '9', 'A' },
  { '4', '5', '6', 'B' },
  { '1', '2', '3', 'C' },
  { '*', '0', '#', 'D' }
};
byte rowPins[ROWS] = { 17, 16, 15, 14 };
byte colPins[COLS] = { 21, 20, 19, 18 };

// Define los patrones para cada número
byte numeros[10][8] = {
  { 0x3c, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x3c },  // 0
  { 0x18, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x38 },  // 1
  { 0x3c, 0x42, 0x40, 0x20, 0x10, 0x08, 0x04, 0x7e },  // 2
  { 0x3c, 0x42, 0x40, 0x40, 0x38, 0x40, 0x42, 0x3c },  // 3
  { 0x44, 0x44, 0x44, 0x7c, 0x40, 0x40, 0x40, 0x40 },  // 4
  { 0x7e, 0x02, 0x02, 0x3c, 0x40, 0x40, 0x40, 0x3e },  // 5
  { 0x3c, 0x42, 0x02, 0x02, 0x3e, 0x42, 0x42, 0x3c },  // 6
  { 0x7e, 0x40, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02 },  // 7
  { 0x3c, 0x42, 0x42, 0x42, 0x3c, 0x42, 0x42, 0x3c },  // 8
  { 0x3c, 0x42, 0x42, 0x7c, 0x40, 0x40, 0x20, 0x10 }   // 9
};

const int DIN_PIN = 51;  // Pin de entrada de datos
const int CS_PIN = 53;   // Pin de selección del chip
const int CLK_PIN = 52;  // Pin de reloj

LedControl lc = LedControl(DIN_PIN, CLK_PIN, CS_PIN, 1);
Keypad keypad = Keypad(makeKeymap(keys), rowPins, colPins, ROWS, COLS);
LiquidCrystal lcd(8, 9, 10, 11, 12, 13);

String comando = "";
unsigned long last = 0;
boolean verificador = 0;
byte modo = 0;  //0 = modo inicial, 1 = modo usuario, 2 = modo admin
boolean verificadorModo = 0;
boolean esperaToken = 0;
String token;
int contadorErrores = 0;
String txtKeypad = "";
String apodo;
String contrasena;
String nombre;
int costoProd;
String nombreProd;
int posicionMenuAdmin = 0;
int contadorUsuariosGuardados = 0;
int idUsuarioActual = 0;

byte bt[8] = {
  B01000,
  B01100,
  B01010,
  B01001,
  B11110,
  B01001,
  B01010,
  B01100
};

struct Usuario {
  String nombre;
  String apodo;
  String contrasena;
  int credito;
};

Usuario usuarios[5];
Usuario admin;
Usuario actual;

struct Producto {
  String nombre;
  int precio;
  int existencias;
};

Producto productos[10];

//Funcion para generar token aleatorio de 6 caracteres---------------------------------------------------------------------------------------------------------------------------------
String generarToken() {
  const char caracteres[] = { 'A', 'B', 'C', 'D', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
  const int numCaracteres = sizeof(caracteres) / sizeof(caracteres[0]);
  const int tamToken = 6;
  char token[tamToken + 1];
  randomSeed(analogRead(A3));
  randomSeed(millis());
  for (int i = 0; i < tamToken; i++) {
    byte randNum = random(0, numCaracteres);
    token[i] = caracteres[randNum];
  }
  token[tamToken] = '\0';

  return String(token);
}

//----------------------------------------FUNCIONES NECESARIAS IMPRIMIR, GUARDAR Y LEER USUARIOS DESDE LA EEPROM--------------------------------------
void imprimirUsuario(Usuario usr) {
  Serial.println("------------------------------------------");
  Serial.print("Nombre: ");
  Serial.println(usr.nombre);

  Serial.print("Apodo: ");
  Serial.println(usr.apodo);

  Serial.print("Contraseña: ");
  Serial.println(usr.contrasena);

  Serial.print("Crédito: ");
  Serial.println(usr.credito);
  Serial.println("------------------------------------------");
}

void guardarUsuario(Usuario usr, int direccion) {
  // Guardar el nombre
  byte len = usr.nombre.length();
  EEPROM.write(direccion, len);
  for (int i = 0; i < len; i++) {
    EEPROM.write(direccion + 1 + i, usr.nombre.charAt(i));
  }

  // Guardar el apodo
  len = usr.apodo.length();
  EEPROM.write(direccion + 1 + usr.nombre.length(), len);
  for (int i = 0; i < len; i++) {
    EEPROM.write(direccion + 1 + usr.nombre.length() + 1 + i, usr.apodo.charAt(i));
  }

  // Guardar la contraseña
  len = usr.contrasena.length();
  EEPROM.write(direccion + 1 + usr.nombre.length() + 1 + usr.apodo.length(), len);
  for (int i = 0; i < len; i++) {
    EEPROM.write(direccion + 1 + usr.nombre.length() + 1 + usr.apodo.length() + 1 + i, usr.contrasena.charAt(i));
  }

  // Guardar el crédito
  EEPROM.put(direccion + 1 + usr.nombre.length() + 1 + usr.apodo.length() + 1 + usr.contrasena.length(), usr.credito);
}

Usuario leerUsuario(int direccion) {
  Usuario usr;

  // Leer el nombre
  byte len = EEPROM.read(direccion);
  char nombre[len + 1];
  for (int i = 0; i < len; i++) {
    nombre[i] = EEPROM.read(direccion + 1 + i);
  }
  nombre[len] = '\0';  // Agregar el terminador nulo al final del string
  usr.nombre = String(nombre);

  // Leer el apodo
  len = EEPROM.read(direccion + 1 + strlen(nombre));
  char apodo[len + 1];
  for (int i = 0; i < len; i++) {
    apodo[i] = EEPROM.read(direccion + 1 + strlen(nombre) + 1 + i);
  }
  apodo[len] = '\0';
  usr.apodo = String(apodo);

  // Leer la contraseña
  len = EEPROM.read(direccion + 1 + strlen(nombre) + 1 + strlen(apodo));
  char contrasena[len + 1];
  for (int i = 0; i < len; i++) {
    contrasena[i] = EEPROM.read(direccion + 1 + strlen(nombre) + 1 + strlen(apodo) + 1 + i);
  }
  contrasena[len] = '\0';
  usr.contrasena = String(contrasena);

  // Leer el crédito
  EEPROM.get(direccion + 1 + strlen(nombre) + 1 + strlen(apodo) + 1 + strlen(contrasena), usr.credito);

  return usr;
}

//----------------------------------------FUNCIONES NECESARIAS IMPRIMIR, GUARDAR Y LEER PRODUCTOS DESDE LA EEPROM--------------------------------------
void imprimirProducto(Producto prod) {
  Serial.println("------------------------------------------");
  Serial.print("Nombre: ");
  Serial.println(prod.nombre);

  Serial.print("Precio: Q");
  Serial.println(prod.precio);

  Serial.print("Existencias: ");
  Serial.println(prod.existencias);
  Serial.println("------------------------------------------");
}

void guardarProducto(Producto prod, int direccion) {
  // Guardar el nombre
  byte len = prod.nombre.length();
  EEPROM.write(direccion, len);
  for (int i = 0; i < len; i++) {
    EEPROM.write(direccion + 1 + i, prod.nombre.charAt(i));
  }

  // Guardar el precio
  EEPROM.put(direccion + 1 + prod.nombre.length(), prod.precio);

  // Guardar las existencias
  EEPROM.put(direccion + 1 + prod.nombre.length() + sizeof(prod.precio), prod.existencias);
}

Producto leerProducto(int direccion) {
  Producto prod;

  // Leer el nombre
  byte len = EEPROM.read(direccion);
  char nombre[len + 1];
  for (int i = 0; i < len; i++) {
    nombre[i] = EEPROM.read(direccion + 1 + i);
  }
  nombre[len] = '\0';  // Agregar el terminador nulo al final del string
  prod.nombre = String(nombre);

  // Leer el precio
  EEPROM.get(direccion + 1 + strlen(nombre), prod.precio);

  // Leer las existencias
  EEPROM.get(direccion + 1 + strlen(nombre) + sizeof(prod.precio), prod.existencias);

  return prod;
}
//---------------------------------------------------------------------------------------------------------------------------------------------------------
void mostrarTexto(String linea1, String linea2, boolean inicio) {
  if (inicio) {
    lcd.clear();          // Limpiamos la pantalla
    lcd.setCursor(0, 0);  // Colocamos el cursor en la primera fila y primera columna
    lcd.write(byte(0));
    lcd.print("Bienvenido");
    lcd.write(byte(0));
    lcd.setCursor(0, 1);  // Colocamos el cursor en la segunda fila y primera columna
    lcd.print(linea2);    // Mostramos la segunda cadena de texto en la pantalla
  } else {
    lcd.clear();          // Limpiamos la pantalla
    lcd.setCursor(0, 0);  // Colocamos el cursor en la primera fila y primera columna
    lcd.print(linea1);    // Mostramos la primera cadena de texto en la pantalla
    lcd.setCursor(0, 1);  // Colocamos el cursor en la segunda fila y primera columna
    lcd.print(linea2);    // Mostramos la segunda cadena de texto en la pantalla
  }
}

void setup() {
  Serial.begin(9600);

  pinMode(led, OUTPUT);

  pinMode(B1, OUTPUT);
  pinMode(B2, OUTPUT);
  pinMode(B3, OUTPUT);
  pinMode(B4, OUTPUT);

  pinMode(btnAceptar, INPUT);
  pinMode(btnCancelar, INPUT);
  pinMode(btnDerecha, INPUT);
  pinMode(btnIzquierda, INPUT);

  lc.shutdown(0, false);  // Inicializa el objeto LedControl
  lc.setIntensity(0, 8);  // Establece el brillo de la matriz de LED
  lc.clearDisplay(0);     // Limpia la matriz de LED

  lcd.begin(16, 2);
  lcd.createChar(0, bt);
  lcd.clear();
  mostrarTexto("", "GRP 07 SEC A", 1);  //---------------------------------

  //AQUI LLENAS LA INFO DEL USUARIO ADMIN
  admin.nombre = "ad07min-a";
  admin.apodo = "A4152F";
  admin.contrasena = "5A0770";
  admin.credito = 0;

  //Leer los productos y guardarlos
  for (int i = 0; i < 10; i++) {
    productos[i] = leerProducto(400 + (i * 50));
  }

  //Leer los usuarios y guardarlos
  for (int i = 0; i < 5; i++) {
    usuarios[i] = leerUsuario(0 + (i * 50));
    if (usuarios[i].credito == 0) {
      contadorUsuariosGuardados = i;
      break;
    }
  }
}

void loop() {
  lecturaSerialBluetooth();
  //--------------------------------------------------------------------------------------------
  char key = keypad.getKey();
  if (esperaToken && key) {

    txtKeypad += key;


    mostrarTexto("token:", txtKeypad, 0);

    if (txtKeypad.length() >= 6) {
      if (txtKeypad == token) {
        if (verificadorModo) {
          modo = 2;
          posicionMenuAdmin = 3;
          mostrarTexto("*Productos", " Usuarios", 0);
        } else {
          modo = 1;
        }
        esperaToken = 0;
      } else {
        contadorErrores++;
        mostrarTexto("ERROR", "-----", 0);
        delay(1000);
        txtKeypad = "";
        mostrarTexto("token:", txtKeypad, 0);
      }
      txtKeypad = "";
    }
  }
  if (contadorErrores >= 3) {
    esperaToken = 0;
    modo = 0;
    for (int i = 10; i >= 0; i--) {
      mostrarTexto("Bloqueado", "seg: " + String(i), 0);
      delay(1000);
    }
    mostrarTexto("", "GRP 07 SEC A", 1);
    contadorErrores = 0;
  }
  //-------------------------------------------------------------------------------------------
  if (modo == 1) {
    Serial.print("1,Apodo: " + String(actual.apodo) + "\nCredito: " + String(actual.credito));
    realizarCompra();
    modo = 0;
    mostrarTexto("", "GRP 07 SEC A", 1);
  }
  //--------------------------------------------------------------------------------------------
  if (modo == 2) {
    if (digitalRead(btnAceptar)) {
      switch (posicionMenuAdmin) {
        case 0:
          menuConfiguracionRegistros();
          break;
        case 1:
          menuConfiguracionUsuarios();
          break;
        case 2:
          //Enviar estatus de la maquina
          //Sacamos el porcentaje
          int cantidadUsuariosCero = 0;
          for (int i = 0; i < contadorUsuariosGuardados; i++) {
            if (usuarios[i].credito <= 0) {
              cantidadUsuariosCero++;
            }
          }
          int porcentajeUsuariosCero = cantidadUsuariosCero * (contadorUsuariosGuardados / 100);
          //Sacamos la cantidad de productos con unidades disponibles y no disponibles
          int cantidadProductosDisp = 0;
          int cantidadProductosNulos = 0;
          for (int i = 0; i < 10; i++) {
            if (productos[i].existencias > 0) {
              cantidadProductosDisp++;
            }
          }
          for (int i = 0; i < 10; i++) {
            if (productos[i].existencias == 0) {
              cantidadProductosNulos++;
            }
          }
          Serial.print("2,Por de usr: " + String(cantidadUsuariosCero) + "\nCt usr: " + String(contadorUsuariosGuardados) + "\nCp disp: " + String(cantidadProductosDisp) + "\nCp nulos: " + String(cantidadProductosNulos));
          break;
      }
      delay(80);
      while (digitalRead(btnAceptar))
        ;
    }
    if (digitalRead(btnCancelar)) {
      modo = 0;
      mostrarTexto("", "GRP 07 SEC A", 1);
      delay(80);
      while (digitalRead(btnCancelar))
        ;
    }
    if (digitalRead(btnDerecha)) {
      posicionMenuAdmin++;
      if (posicionMenuAdmin > 2) {
        posicionMenuAdmin = 0;
      }
      switch (posicionMenuAdmin) {
        case 0:
          mostrarTexto("*Productos", " Usuarios", 0);
          break;
        case 1:
          mostrarTexto(" Productos", "*Usuarios", 0);
          break;
        case 2:
          mostrarTexto(" Usuarios", "*Estatus", 0);
          break;
      }
      delay(80);
      while (digitalRead(btnDerecha))
        ;
    }
    if (digitalRead(btnIzquierda)) {
      posicionMenuAdmin--;
      if (posicionMenuAdmin < 0) {
        posicionMenuAdmin = 2;
      }
      switch (posicionMenuAdmin) {
        case 0:
          mostrarTexto("*Productos", " Usuarios", 0);
          break;
        case 1:
          mostrarTexto(" Productos", "*Usuarios", 0);
          break;
        case 2:
          mostrarTexto(" Usuarios", "*Estatus", 0);
          break;
      }
      delay(80);
      while (digitalRead(btnIzquierda))
        ;
    }
  }
}

//-----------------------------------------------------------------------------------------------------------------------------
void realizarCompra() {
  //Leer los productos y guardarlos
  for (int i = 0; i < 10; i++) {
    productos[i] = leerProducto(400 + (i * 50));
  }
  int posicionMenuProductos = 0;
  lc.clearDisplay(0);  // Limpia la matriz de LED
  delay(100);          // Espera 0.1 segundos antes de mostrar el siguiente número
  for (int j = 0; j < 8; j++) {
    lc.setRow(0, j, numeros[posicionMenuProductos][j]);  // Muestra el número en la matriz de LED
  }
  mostrarTexto("CARGANDO...", "", 0);
  delay(1000);
  mostrarTexto(productos[posicionMenuProductos].nombre, "Q" + String(productos[posicionMenuProductos].precio) + " E: " + String(productos[posicionMenuProductos].existencias), 0);
  while (1) {
    if (digitalRead(btnAceptar)) {

      if (productos[posicionMenuProductos].existencias != 0) {
        mostrarTexto("CARGANDO...", "", 0);
        delay(1000);
        mostrarTexto("Digite la cantidad", "", 0);
        txtKeypad = "";
        while (1) {
          char key = keypad.getKey();
          if (key == '0' || key == '1' || key == '2' || key == '3' || key == '4' || key == '5' || key == '6' || key == '7' || key == '8' || key == '9') {
            txtKeypad += key;
            mostrarTexto("Digite la cantidad", txtKeypad, 0);
          }
          if (digitalRead(btnAceptar)) {
            if (txtKeypad.toInt() > 0 && txtKeypad.toInt() < productos[posicionMenuProductos].existencias) {
              if (actual.credito > (productos[posicionMenuProductos].precio * txtKeypad.toInt())) {
                //Compra exitosa :)
                actual.credito = actual.credito - productos[posicionMenuProductos].precio * txtKeypad.toInt();
                productos[posicionMenuProductos].existencias = productos[posicionMenuProductos].existencias - txtKeypad.toInt();
                mostrarTexto("COMPRA EXITOSA", "", 0);
                guardarUsuario(actual, idUsuarioActual * 50);
                guardarProducto(productos[posicionMenuProductos], 400 + (posicionMenuProductos * 50));
                Serial.print("1,Id usr: " + String(idUsuarioActual) + "\nApodo: " + String(actual.apodo) + "\nCredito: " + String(actual.credito));
                for (int i = 0; i < txtKeypad.toInt(); i++) {
                  vueltaMotor();
                  delay(200);
                }
              } else {
                mostrarTexto("ERROR SALDO", "", 0);
                delay(1000);
              }
            } else {
              mostrarTexto("ERROR EXISTENCIA", "", 0);
              delay(1000);
            }
            delay(80);
            while (digitalRead(btnAceptar))
              ;
            txtKeypad = "";
            mostrarTexto("CARGANDO...", "", 0);
            delay(1000);
            mostrarTexto(productos[posicionMenuProductos].nombre, "Q" + String(productos[posicionMenuProductos].precio) + " E: " + String(productos[posicionMenuProductos].existencias), 0);
            break;
          }
          if (digitalRead(btnCancelar)) {
            delay(80);
            while (digitalRead(btnCancelar))
              ;
            break;
          }
        }
      }

      delay(80);
      while (digitalRead(btnAceptar))
        ;
    }
    if (digitalRead(btnCancelar)) {
      break;
      delay(80);
      while (digitalRead(btnCancelar))
        ;
    }
    if (digitalRead(btnDerecha)) {

      posicionMenuProductos++;
      if (posicionMenuProductos > 9) {
        posicionMenuProductos = 0;
      }

      lc.clearDisplay(0);  // Limpia la matriz de LED
      delay(100);          // Espera 0.1 segundos antes de mostrar el siguiente número
      for (int j = 0; j < 8; j++) {
        lc.setRow(0, j, numeros[posicionMenuProductos][j]);  // Muestra el número en la matriz de LED
      }
      mostrarTexto(productos[posicionMenuProductos].nombre, "Q" + String(productos[posicionMenuProductos].precio) + " E: " + String(productos[posicionMenuProductos].existencias), 0);

      while (digitalRead(btnDerecha))
        ;
    }
    if (digitalRead(btnIzquierda)) {

      posicionMenuProductos--;
      if (posicionMenuProductos < 0) {
        posicionMenuProductos = 9;
      }
      lc.clearDisplay(0);  // Limpia la matriz de LED
      delay(100);          // Espera 0.1 segundos antes de mostrar el siguiente número
      for (int j = 0; j < 8; j++) {
        lc.setRow(0, j, numeros[posicionMenuProductos][j]);  // Muestra el número en la matriz de LED
      }
      mostrarTexto(productos[posicionMenuProductos].nombre, "Q" + String(productos[posicionMenuProductos].precio) + " E: " + String(productos[posicionMenuProductos].existencias), 0);

      while (digitalRead(btnIzquierda))
        ;
    }
  }
  lc.clearDisplay(0);  // Limpia la matriz de LED
}
//-----------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------
void vueltaMotor() {
  int tiempo = 50;
  setMotorPines(0, 0, 0, 1);
  delay(tiempo);
  setMotorPines(0, 0, 1, 0);
  delay(tiempo);
  setMotorPines(0, 1, 0, 0);
  delay(tiempo);
  setMotorPines(1, 0, 0, 0);
  delay(tiempo);
}
void setMotorPines(int a, int b, int c, int d) {
  digitalWrite(B1, a);
  digitalWrite(B2, b);
  digitalWrite(B3, c);
  digitalWrite(B4, d);
}
//--------------------------------------------------------------------------------------------------------------------------
void menuConfiguracionRegistros() {
  delay(1000);
  //Leer los productos y guardarlos
  for (int i = 0; i < 10; i++) {
    productos[i] = leerProducto(400 + (i * 50));
  }
  int posicionMenuProductos = 0;
  lc.clearDisplay(0);  // Limpia la matriz de LED
  delay(100);          // Espera 0.1 segundos antes de mostrar el siguiente número
  for (int j = 0; j < 8; j++) {
    lc.setRow(0, j, numeros[posicionMenuProductos][j]);  // Muestra el número en la matriz de LED
  }
  mostrarTexto("CARGANDO...", "", 0);
  delay(1000);
  mostrarTexto(productos[posicionMenuProductos].nombre, "Q" + String(productos[posicionMenuProductos].precio) + " E: " + String(productos[posicionMenuProductos].existencias), 0);
  while (1) {
    lecturaSerialBluetooth();
    if (digitalRead(btnAceptar)) {

      if (productos[posicionMenuProductos].existencias == 0) {
        modificacionProducto(posicionMenuProductos);
        lc.clearDisplay(0);  // Limpia la matriz de LED
        delay(100);          // Espera 0.1 segundos antes de mostrar el siguiente número
        for (int j = 0; j < 8; j++) {
          lc.setRow(0, j, numeros[posicionMenuProductos][j]);  // Muestra el número en la matriz de LED
        }
        mostrarTexto(productos[posicionMenuProductos].nombre, "Q" + String(productos[posicionMenuProductos].precio) + " E: " + String(productos[posicionMenuProductos].existencias), 0);
      }

      delay(80);
      while (digitalRead(btnAceptar))
        ;
    }
    if (digitalRead(btnCancelar)) {
      break;
      delay(80);
      while (digitalRead(btnCancelar))
        ;
    }
    if (digitalRead(btnDerecha)) {

      posicionMenuProductos++;
      if (posicionMenuProductos > 9) {
        posicionMenuProductos = 0;
      }

      lc.clearDisplay(0);  // Limpia la matriz de LED
      delay(100);          // Espera 0.1 segundos antes de mostrar el siguiente número
      for (int j = 0; j < 8; j++) {
        lc.setRow(0, j, numeros[posicionMenuProductos][j]);  // Muestra el número en la matriz de LED
      }
      mostrarTexto(productos[posicionMenuProductos].nombre, "Q" + String(productos[posicionMenuProductos].precio) + " E: " + String(productos[posicionMenuProductos].existencias), 0);

      while (digitalRead(btnDerecha))
        ;
    }
    if (digitalRead(btnIzquierda)) {

      posicionMenuProductos--;
      if (posicionMenuProductos < 0) {
        posicionMenuProductos = 9;
      }
      lc.clearDisplay(0);  // Limpia la matriz de LED
      delay(100);          // Espera 0.1 segundos antes de mostrar el siguiente número
      for (int j = 0; j < 8; j++) {
        lc.setRow(0, j, numeros[posicionMenuProductos][j]);  // Muestra el número en la matriz de LED
      }
      mostrarTexto(productos[posicionMenuProductos].nombre, "Q" + String(productos[posicionMenuProductos].precio) + " E: " + String(productos[posicionMenuProductos].existencias), 0);

      while (digitalRead(btnIzquierda))
        ;
    }
  }
  lc.clearDisplay(0);  // Limpia la matriz de LED
}
//--------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------
void lecturaSerialBluetooth() {
  //--------------------------------------------------------------------------------------------
  //En caso de recibir un mensaje bluetooth
  while (Serial.available()) {
    char dato = Serial.read();

    if (dato != '.') {
      comando += dato;  //*fewrfrkgr-efjwerifjeri
      digitalWrite(led, 1);
    } else {
      //Serial.println(comando);

      char valor = comando.charAt(0);

      if (valor == '*' && modo == 0) {
        apodo = comando.substring(1, comando.indexOf('-'));
        contrasena = comando.substring(comando.indexOf('-') + 1, comando.length());
        apodo.trim();
        contrasena.trim();

        if (apodo == admin.apodo && contrasena == admin.contrasena) {
          verificadorModo = 1;
          token = generarToken();
          Serial.print("3," + token);
          esperaToken = 1;
          mostrarTexto("token:", "", 0);
        } else {
          for (int i = 0; i < 5; i++) {
            if (apodo == usuarios[i].apodo && contrasena == usuarios[i].contrasena) {
              verificadorModo = 0;
              token = generarToken();
              Serial.print("3," + token);
              esperaToken = 1;
              mostrarTexto("token:", "", 0);
              actual.nombre = usuarios[i].nombre;
              actual.apodo = apodo;
              actual.contrasena = usuarios[i].contrasena;
              actual.credito = usuarios[i].credito;
              idUsuarioActual = i;
              Serial.print("1,Id usr: " + String(idUsuarioActual) + "\nApodo: " + String(actual.apodo) + "\nCredito: " + String(actual.credito));
              break;
            }
          }
        }
      }

      if (valor == '+' && modo == 2) {
        nombreProd = comando.substring(1, comando.indexOf('-'));
        costoProd = comando.substring(comando.indexOf('-') + 1, comando.length()).toInt();
        nombreProd.trim();
      }

      if (valor == '!' && modo == 2) {
        nombre = comando.substring(1, comando.indexOf('-'));
        apodo = comando.substring(comando.indexOf('-') + 1, comando.indexOf('?'));
        contrasena = comando.substring(comando.indexOf('?') + 1, comando.length());
        nombre.trim();
        apodo.trim();
        contrasena.trim();
      }
      digitalWrite(led, 0);
      comando = "";
    }
  }
  //--------------------------------------------------------------------------------------------
}
//--------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------
void modificacionProducto(int producto) {
  

  mostrarTexto("CARGANDO...", "", 0);
  delay(1000);
  mostrarTexto("Nom y costo app", "", 0);
  while (1) {
    if (digitalRead(btnAceptar)) {
      mostrarTexto("cambio guardado", "....", 0);
      productos[producto].nombre = nombreProd;
      productos[producto].precio = costoProd;
      productos[producto].existencias = 10;
      guardarProducto(productos[producto], 400 + (producto * 50));
      delay(1000);
      break;
      while (digitalRead(btnAceptar))
        ;
    }
    if (digitalRead(btnCancelar)) {
      break;
      delay(80);
      while (digitalRead(btnCancelar))
        ;
    }
    lecturaSerialBluetooth();
  }
}
//--------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------
void menuConfiguracionUsuarios() {
  mostrarTexto("CARGANDO...", "", 0);
  delay(1000);
  mostrarTexto("envia desde app", "", 0);
  while (1) {
    if (digitalRead(btnAceptar)) {
      mostrarTexto("cambio guardado", "....", 0);
      usuarios[contadorUsuariosGuardados].nombre = nombre;
      usuarios[contadorUsuariosGuardados].contrasena = contrasena;
      usuarios[contadorUsuariosGuardados].apodo = apodo;
      usuarios[contadorUsuariosGuardados].credito = 250;
      guardarUsuario(usuarios[contadorUsuariosGuardados], contadorUsuariosGuardados * 50);
      contadorUsuariosGuardados++;
      delay(1000);
      modo = 0;
      mostrarTexto("", "GRP 07 SEC A", 1);
      break;
      while (digitalRead(btnAceptar))
        ;
    }
    if (digitalRead(btnCancelar)) {
      break;
      delay(80);
      while (digitalRead(btnCancelar))
        ;
    }
    lecturaSerialBluetooth();
  }
}
//--------------------------------------------------------------------------------------------------------------------------