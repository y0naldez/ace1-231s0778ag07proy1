# **Manual de Tecnico Practica 1 ACE1** 
## **Introducción**
<div style="text-align: justify"> 
En este documento se describe los aspectos técnicos informáticos, de manera que cualquier técnico informático pueda entender y comprender la lógica dentro del programa, para así poder darle mantenimiento y actualizarla si es necesario.

</div>


## **Objetivos**
<div style="text-align: justify"> 

Instruir al lector para el uso adecuado del código del programa, así mismo dar a conocer la lógica detrás del programa, de manera que se le facilite al lector la manipulación del código del programa.
</div>



## **Conocimientos Previos**
<div style="text-align: justify"> 

Los conocimientos que deberán tener las personas que manejen el programa son:
-   Manejo de IDE Visual Studio Code.
-	Logica de programación.
-	Conocimiento de lenguaje c++.
-   Proteus
</div>

## **Requisitos del sistema**
- CPU, Intel Core 2 Duo 2 GHz recomendado
- RAM, 2 GB recomendado 
- Espacio en el disco duro, 30 mb
- Sistema Operativo windows 7,8,9.

### **Variables y librerias** 
<div style="text-align: justify"> 
Se muestra la definicion variables que se utilizaron.

![Esta es una imagen de ejemplo](1.PNG)
</div>

### **Patrones** 
<div style="text-align: justify"> 
Por medio de un arreglo se definieron los patrones establecidos para cada numero.

![Esta es una imagen de ejemplo](3.PNG)
</div>

## **Funcion token** 
<div style="text-align: justify"> 
Funcion para generar el token aleatorio que contiene 6 caracteres y se muestra al usuario para su respectiva verificación.

![Esta es una imagen de ejemplo](Token.PNG)
</div>

### **Funciones para imprimir.** 
<div style="text-align: justify"> 
Permite imprimir las credenciales del usuario nombre, apodo, contraseña y el credito que tiene disponible.

![Esta es una imagen de ejemplo](ImprimirUser.PNG)
</div>

### **Usuario  funciones varias** 
Funcion que permite guardar los datos de un usuario cuando este esta siendo registrado.
![Esta es una imagen de ejemplo](guardarUser.PNG)

Funcion que permite guardar los datos de un usuario cuando este esta siendo registrado.
![Esta es una imagen de ejemplo](guardarApodo.PNG)

Esta funcion permite almacenar el credito que el usuario tiene disponible.
![Esta es una imagen de ejemplo](guardarCredito.PNG)

### **Fuciones para leer** 
<div style="text-align: justify"> 
En esta parte se encuentran varias funciones las siguientes:
Leer el nombre del usuario,
Leer el apodo del asuario,
Leer la contraseña del usuario.

![Esta es una imagen de ejemplo](LeerTodo.PNG)
</div>




### **Funciones para imprimir** 
<div style="text-align: justify"> 
Estas son las funciones necesarias para imprimir y leer productos desde la EEPROM las cuales son imprimir producto, guardar un producto, guardar el precio de un nuevo producto y guardar las existencias del producto.

![Esta es una imagen de ejemplo](EEPROM.PNG)
</div>

### **Funciones para mostrar texto** 
<div style="text-align: justify"> 
Funcion que permite limpiar la pantalla y pode mostar un texto nuevo segun lo que se requiera.

![Esta es una imagen de ejemplo](Show%20text.PNG)
</div>



### **Funciones para mostrar texto** 
<div style="text-align: justify"> 
While que permite modicicar un producto al administrador.

![Esta es una imagen de ejemplo](ModifyProduct.PNG)
</div>

### **Configuraciones** 
Metodo que permite configurar los datos de un usuario desde el perfil del administrador.

![Esta es una imagen de ejemplo](ConfigUsuarios.PNG)
</div>


### **Lectura Serial** 
<div style="text-align: justify"> 
Permite al usuario recibir un mensaje por medio de bluetooth.

![Esta es una imagen de ejemplo](LecturaSerial.PNG)
</div>

### **Motor** 
<div style="text-align: justify"> 
Metodo para poder indicar las vueltas al motor.

![Esta es una imagen de ejemplo](VueltasMotor.PNG)
</div>




### **Codigo de implementación de pantalla** 
<div style="text-align: justify"> 
Metodo para poder indicar las vueltas al motor.

![Esta es una imagen de ejemplo](Code1.PNG)
</div>


### **Solicitud de permisos y conexión bt.** 
<div style="text-align: justify"> 
Metodo para poder indicar las vueltas al motor.

![Esta es una imagen de ejemplo](Code2.PNG)
</div>



### ** Validación de contraseña** 
<div style="text-align: justify"> 
Metodo para poder indicar las vueltas al motor.

![Esta es una imagen de ejemplo](Code4.PNG)
</div>