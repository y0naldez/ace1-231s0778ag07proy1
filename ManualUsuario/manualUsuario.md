# **Manual de usuario  Proyecto 1 Arquitectura de y ensambladores 2 ** 


 
<div style="text-align: justify "> En esta guía de usuario se ofrece una visión general a las características de la aplicación y se indican las instrucciones que deben seguirse paso a paso para realizar diversas tareas.  El software cuenta con una interfaz intuitiva, pero se asume que el usuario estará ya familiarizado con los términos, conceptos y métodos presentados en los Indicadores técnicos previstos. </div>


### **Carga de instrucciones** 

<div style="text-align: justify"> 1. Doble click sobre el IDE Arduino. 

![Esta es una imagen de ejemplo](1.JPG)
</div>

<div style="text-align: justify">2. Click en file y open. Se desplegará una ventana emergente para seleccionar el archivo. 

![Esta es una imagen de ejemplo](2.JPG)


![Esta es una imagen de ejemplo](3.jpg)
 </div>


### **Conexión con la aplicación **
1.Para conectarse a la aplicación es necesario establecer una conexión bluethooh con el ordenador. Es necesario configurar un puerto COM entrante y colocarlo en las configuraciones del arduino.  
2. En la aplicación es necesario seleccionar al ordenador en el apartado de "dispositivos". Una conexión exitosa redirigirá al usuario al menú principal de la aplicación.

 <div style="text-align: justify"> 

![Esta es una imagen de ejemplo](5.jpg)

 </div>
<div style="text-align: justify"> 

![Esta es una imagen de ejemplo](6.jpg)

 </div>


### **Funcionalidades** 

**Vista general de las funcionalidades.**
 <div style="text-align: justify"> 
 

 </div>

**Mensaje de bienvenida.**
 <div style="text-align: justify"> 
 Al iniciar el programa se mostrará el siguiente mensaje de bienvenida y la aplicación estará a la espera de una conexión con el ordenador.

![Esta es una imagen de ejemplo](4.jpg)

 </div>




 <div style="text-align: justify"> 
 -

 </div>



**ADMINISTRADOR.**

**Iniciar sesión.**
<div style="text-align: justify"> 
 
Las credenciales para iniciar sesión a un usuario de tipo administrador estan establecidad desde el inicio y son las siguientes:
**apodo: A4152F**
**contraseña: 5A0770**

Si las credenciales correctas se envirá un token a la aplicación el cual se debe de ingresar:

![Esta es una imagen de ejemplo](tokenIn.jpg)

![Esta es una imagen de ejemplo](tk.jpg)

Si el token es correcto será redirigido al menú de administrador de lo contrario saltará en pantalla un mensaje indicando ERROR.

![Esta es una imagen de ejemplo](error.jpg)

Se cuentan con dos intentos más para ingresar el token. Si los intentos restantes son fallidos se generará un bloqueo por 10 segundos y  se procederá a redirigir al usuario al menú inicial.

![Esta es una imagen de ejemplo](bloqueo.jpg)



</div>




**Registrar un usuario.**

<div style="text-align: justify"> 

Para registrar un usuario se pedirá que el usuario envie los datos por medio de la app. Un mensaje en pantalla podrá ser visualizado indicando lo anterior  
![Esta es una imagen de ejemplo](usr.jpg)

![Esta es una imagen de ejemplo](usr2.jpg)

Desde la app se deben de ingresar los campos de: **Nombre,Apodo,Contraseña**

![Esta es una imagen de ejemplo](usrR.jpg)

Cuando los datos sean enviados desde la app se deberá de presionar el botón aceptar en el ordenador.

![Esta es una imagen de ejemplo](aceptar.jpg)

Un registro estará delimitado por: 
![Esta es una imagen de ejemplo](cambioG.jpg)
</div>


**Modificar un producto.**

<div style="text-align: justify"> 

Para modificar un producto se deberá elegir un producto que cumpla con la siguiente condición: **existencia = 0**.

Para recorrer la lista de productos disponibles es necesario la utilización de los botones derecha e izquierda.

![Esta es una imagen de ejemplo](driz.jpg)

Un panel indicará el número de producto seleccionado.

![Esta es una imagen de ejemplo](num.jpg)

La pantalla LCD por su parte mostrará la información del producto seleccionado.

![Esta es una imagen de ejemplo](infoPro.jpg)

Al elegir un producto que cumpla la condición anteriormente descrita un mensaje en pantalla pedirá el ingreso de datos desde la app.

![Esta es una imagen de ejemplo](nomYcos.jpg)

![Esta es una imagen de ejemplo](modificarP.jpg)


Una modificación estará delimitado por: 
![Esta es una imagen de ejemplo](cambioG.jpg)

Por defecto se agregarán 10 existencias al nuevo producto.

![Esta es una imagen de ejemplo](trident.jpg)
</div>


**Estatus del sistema.**

<div style="text-align: justify"> 

Se desplegará automaticamente en la app la siguiente información.

– M0: Porcentaje de usuarios que se quedaron sin crédito
– M1: Cantidad total de usuarios
– M2: Cantidad de productos con unidades disponibles
– M3: Cantidad de productos nulos o con existencia cero.

![Esta es una imagen de ejemplo](infoSis.jpg)

</div>


**USUARIO.**
<div style="text-align: justify"> 


 

</div>

**Iniciar sesión.**
<div style="text-align: justify"> 
 
Las credenciales para iniciar sesión con un usuario sin privilegios de administración son establecidas por el administrador.

Si las credenciales correctas se envirá un token a la aplicación el cual se debe de ingresar:

![Esta es una imagen de ejemplo](tokenIn.jpg)

![Esta es una imagen de ejemplo](tk.jpg)

Si el token es correcto será redirigido al menú de administrador de lo contrario saltará en pantalla un mensaje indicando ERROR.

![Esta es una imagen de ejemplo](error.jpg)

Se cuentan con dos intentos más para ingresar el token. Si los intentos restantes son fallidos se generará un bloqueo por 10 segundos y  se procederá a redirigir al usuario al menú inicial.

![Esta es una imagen de ejemplo](bloqueo.jpg)


</div>

**Información de usuario.**
<div style="text-align: justify"> 
 
Al ingresar correctamente con un usuario la siguiente información será desplegada en la app: **Apodo, Credito**


![Esta es una imagen de ejemplo](infoUsr.jpg)

</div>

**Compra de productos.**

<div style="text-align: justify"> 
 
Para recorrer la lista de productos disponibles es necesario la utilización de los botones derecha e izquierda.

![Esta es una imagen de ejemplo](driz.jpg)

Un panel indicará el número de producto seleccionado.

![Esta es una imagen de ejemplo](num.jpg)

La pantalla LCD por su parte mostrará la información del producto seleccionado.

![Esta es una imagen de ejemplo](infoPro.jpg)

Si se desea comprar un producto es necesario seleccionarlo , si se selecciona un producto con existencia el sistema lo obviará.

![Esta es una imagen de ejemplo](trident.jpg)

Se pedirá que se ingrese la cantidad a comprar.
![Esta es una imagen de ejemplo](cantidad.jpg)

![Esta es una imagen de ejemplo](cantidad2.jpg)

Una compra existosa se delimitará en la aplicación  por el estatus del usuario en sus creditos disponibles y en la aplicación de escritorio en sus existencias. Además el mecanismo de despacho girará la cantidad de veces que se comrpó un producto.


![Esta es una imagen de ejemplo](mecanismo.jpg)

![Esta es una imagen de ejemplo](trident2.jpg)

![Esta es una imagen de ejemplo](trident3.jpg)

**Si una compra no fue posible se lanzará el porqué en la pantalla lcd.**

![Esta es una imagen de ejemplo](existencia.jpg)

![Esta es una imagen de ejemplo](errorSaldo.jpg)


</div>